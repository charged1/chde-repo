#!/usr/bin/env bash

echo "####################################################"
echo "## Adding the CHDE repository to /etc/pacman.conf ##"
echo "####################################################"
grep -qxF "[ariz-core-repo]" /etc/pacman.conf ||
  (echo "[ariz-core-repo]"; echo "SigLevel = Optional DatabaseOptional"; \
  echo "Server = https://gitlab.com/arizos/ariz-repos/\$repo/-/raw/main/\$arch") | sudo tee -a /etc/pacman.conf
